import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zog_project_test/auth/login.dart';
import 'package:zog_project_test/state/color_mode.dart';
import 'package:zog_project_test/utils/constant.dart';
import 'auth/signup.dart';
import 'components/button.dart';
import 'components/text_field.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: true,
      home: BlocProvider<ColorMode>(
        create: (context) => ColorMode(),
        child: Scaffold(
          body: LandingPage(),
        ),
      ),
      // home: Scaffold(
      //   body: LandingPage(),
      // ),
    );
  }
}

class LandingPage extends StatefulWidget {
  LandingPage({Key key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int _landingState = 0;

  bool _darModeState = false;

  var _backgroundColor = Colors.white;
  var _navText = "";

  double _containerHeight = 0;

  double _loginYOffset = 0;
  double _signupYOffset = 0;

  @override
  Widget build(BuildContext context) {
    _containerHeight = MediaQuery.of(context).size.height;
    Size size = MediaQuery.of(context).size;

    // ignore: close_sinks
    ColorMode bloc = BlocProvider.of<ColorMode>(context);

    if (!_darModeState) {
      bloc.add(ColorEvent.primary);
    } else {
      bloc.add(ColorEvent.secondary);
    }

    switch (_landingState) {
      case 0:
        _backgroundColor = Colors.white;

        _loginYOffset = _containerHeight;
        _signupYOffset = _containerHeight;
        break;
      case 1:
        _backgroundColor = cPrimary;

        _loginYOffset = 105;
        _signupYOffset = _containerHeight;

        _navText = "Log In";
        break;
      case 2:
        _backgroundColor = cPrimary;

        _signupYOffset = 105;
        _loginYOffset = _containerHeight;

        _navText = "Sign Up";
        break;
    }

    return Stack(
      children: <Widget>[
        Container(
          height: size.height,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _landingState != 0
                    ? BlocBuilder<ColorMode, Color>(
                        builder: (context, currentColor) => Container(
                          color: currentColor,
                          height: size.height,
                          width: size.width,
                          child: Column(children: [
                            Padding(
                              padding: EdgeInsets.only(
                                top: 50,
                                left: 10,
                              ),
                              child: Container(
                                child: ButtonBack(
                                    text: _navText,
                                    onPressed: () {
                                      setState(() {
                                        _landingState = 0;
                                        FocusScope.of(context).unfocus();
                                      });
                                    }),
                              ),
                            ),
                          ]),
                        ),
                      )
                    : Container(
                        child: Column(children: [
                        Container(
                          height: size.height / 2,
                          width: size.width,
                          child: Center(
                            child: Text(
                              "Welcome to the Apps",
                              style: TextStyle(fontSize: 22),
                            ),
                          ),
                        ),
                        Container(
                          height: size.height / 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              ButtonRounded(
                                  onPressed: () {
                                    setState(() {
                                      _landingState != 0
                                          ? _landingState = 0
                                          : _landingState = 1;
                                    });
                                  },
                                  width: 0.4,
                                  text: Text("Login",
                                      style: TextStyle(color: Colors.white))),
                              ButtonRounded(
                                  onPressed: () {
                                    setState(() {
                                      _landingState != 0
                                          ? _landingState = 0
                                          : _landingState = 2;
                                    });
                                  },
                                  width: 0.4,
                                  text: Text("Sign Up",
                                      style: TextStyle(color: Colors.white)))
                            ],
                          ),
                        ),
                        Container(
                          width: size.width,
                          height: size.height / 6,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Text("Color Primary"),
                              ),
                              Switch(
                                onChanged: (bool value) {
                                  setState(() {
                                    _darModeState = value;
                                  });
                                },
                                value: _darModeState,
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Text("Color Secondary"),
                              )
                            ],
                          ),
                        )
                      ]))
              ],
            ),
          ),
        ),
        LoginPage(
          loginYOfset: _loginYOffset,
        ),
        SignUpPage(
          signupYOfset: _signupYOffset,
        ),
      ],
    );
  }
}
