import 'package:flutter/material.dart';
import 'package:zog_project_test/utils/constant.dart';

class TextFieldCustom extends StatefulWidget {
  final String hint;
  final double width;
  final TextInputType keyboardType;
  final Widget suffixIcon;
  final bool obscureText;

  const TextFieldCustom({
    Key key,
    this.hint,
    this.keyboardType,
    this.suffixIcon,
    this.width,
    this.obscureText,
  }) : super(key: key);

  @override
  _TextFieldCustomState createState() => _TextFieldCustomState();
}

class _TextFieldCustomState extends State<TextFieldCustom> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        width: size.width * widget.width,
        height: 50,
        child: TextFormField(
          autofocus: true,
          keyboardType: widget.keyboardType,
          obscureText: widget.obscureText == true ? widget.obscureText : false,
          decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(6),
                  borderSide: BorderSide(color: Colors.grey[300], width: 1.0)),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(6.0),
                borderSide: BorderSide(
                  color: Colors.grey[400],
                ),
              ),
              filled: true,
              fillColor: lightGray,
              hintText: widget.hint,
              hintStyle: TextStyle(
                color: Colors.grey[400],
                height: 3.0,
              ),
              suffixIcon: widget.suffixIcon),
          validator: (val) => val.length < 8 ? 'Password too short.' : null,
        ));
  }
}
