import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:zog_project_test/components/button.dart';
import 'package:zog_project_test/components/checkbox.dart';
import 'package:zog_project_test/components/text_field.dart';
import 'package:zog_project_test/utils/constant.dart';

// ignore: must_be_immutable
class SignUpPage extends StatefulWidget {
  double signupYOfset;

  SignUpPage({Key key, this.signupYOfset}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  // Initially password is obscure
  bool _obscureText = true;

  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        setState(() {
          FocusScope.of(context).unfocus();
        });
      },
      child: AnimatedContainer(
        duration: Duration(
          milliseconds: 1000,
        ),
        height: size.height,
        curve: Curves.fastLinearToSlowEaseIn,
        transform: Matrix4.translationValues(0, widget.signupYOfset, 1),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(35),
              topRight: Radius.circular(35),
            )),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: 20,
                  left: 80,
                  right: 80,
                  bottom: 20,
                ),
                child: Center(
                  child: Image.asset("assets/img/bg-landing.png"),
                ),
              ),
              Column(
                children: <Widget>[
                  Container(
                      width: size.width,
                      height: size.height * .11,
                      alignment: Alignment.centerLeft,
                      child: Center(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Username",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              TextFieldCustom(
                                width: 0.88,
                                keyboardType: TextInputType.name,
                                hint: "username",
                              ),
                            ]),
                      )),
                  Container(
                      width: size.width,
                      height: size.height * .11,
                      alignment: Alignment.centerLeft,
                      child: Center(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "E-mail",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              TextFieldCustom(
                                width: 0.88,
                                keyboardType: TextInputType.emailAddress,
                                hint: "example@email.com",
                              ),
                            ]),
                      )),
                  Container(
                      width: size.width * .86,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Password",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                            TextFieldCustom(
                              width: 0.88,
                              obscureText: _obscureText,
                              hint: "at least 8 characters",
                              suffixIcon: IconButton(
                                icon: _obscureText == true
                                    ? const Icon(
                                        Icons.remove_red_eye_outlined,
                                        color: Colors.grey,
                                      )
                                    : const Icon(
                                        Icons.remove_red_eye,
                                        color: Colors.grey,
                                      ),
                                tooltip: 'Increase volume by 10',
                                onPressed: () {
                                  setState(() {
                                    _toggle();
                                  });
                                },
                              ),
                            ),
                          ])),
                  Container(
                    width: size.width * .86,
                    child: Row(
                      children: <Widget>[
                        CheckBox(),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 5,
                          ),
                          child: Text.rich(TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "I agree with ",
                              ),
                              TextSpan(
                                text: "Terms",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: cPrimary,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {},
                              ),
                              TextSpan(
                                text: " and ",
                              ),
                              TextSpan(
                                text: "Privacy",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: cPrimary,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {},
                              ),
                            ],
                          )),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 30,
                    ),
                    child: ButtonRounded(
                      width: .86,
                      text: Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
              Container(
                width: size.width,
                margin: EdgeInsets.only(
                  top: 60,
                  bottom: 0,
                ),
                child: Center(
                  child: Text.rich(TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: "I'm already have an account ",
                      ),
                      TextSpan(
                        text: "Sign In",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: cPrimary,
                        ),
                        recognizer: TapGestureRecognizer()..onTap = () {},
                      ),
                    ],
                  )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
