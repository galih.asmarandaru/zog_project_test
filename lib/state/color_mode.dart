import 'package:bloc/bloc.dart'; // dengan package flutter_bloc event dan state sudah dibuatkan
import 'package:flutter/material.dart';
import 'package:zog_project_test/utils/constant.dart';

enum ColorEvent { secondary, primary }

class ColorMode extends Bloc<ColorEvent, Color> {
  Color _color = cPrimary;

  ColorMode() : super(cSecondary);

  @override
  Stream<Color> mapEventToState(ColorEvent event) async* {
    _color = (event == ColorEvent.secondary) ? cSecondary : cPrimary;
    yield _color; // yield adalah perintah untuk memasukan data ke dalam stream
  }
}
